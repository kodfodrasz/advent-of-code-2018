﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day2 {
    public class SolverForDay2 : SolverBase {
        public SolverForDay2() : base("Inventory Management System") { }

        public override IEnumerable<object> Solve(string input) {
            var data = input.Split('\n').Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.Trim()).ToImmutableArray();

            yield return Answer1(data);
            yield return Answer2(data);
        }

        private static int Answer1(ImmutableArray<string> data) {
            (bool hasDoubles, bool hasTriples) ProcessId(string id) {
                var groups = id.GroupBy(c => c).ToImmutableArray();

                bool hasDoubles = groups.Any(g => g.Count() == 2);
                bool hasTriples = groups.Any(g => g.Count() == 3);

                return (hasDoubles, hasTriples);
            }

            var flags = data.Select(ProcessId).ToImmutableArray();

            var doubles = flags.Count(f => f.hasDoubles);
            var triples = flags.Count(f => f.hasTriples);

            return doubles * triples;
        }

        public static string Answer2(ImmutableArray<string> data) {

            var similar = from a in data
                              // Take advantage of each nput being unique. Skip(1) skips the spur of the matrix.
                          let subsequence = data.SkipWhile(x => x != a).Skip(1)
                          from b in subsequence
                          where a.HammingDistance(b) == 1
                          select (a, b);


            var singlePair = similar.Single();

            var ans = singlePair.a.Zip(singlePair.b, Tuple.Create)
                      .Where(t => t.Item1 == t.Item2)
                      .Select(t => t.Item1);

            return new string(ans.ToArray());
        }
    }
}

