﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day8
{
    public class SolverForDay8 : SolverBase
    {
        public SolverForDay8() : base("Memory Maneuver") { }

        public override IEnumerable<object> Solve(string input)
        {
            var data = input.Split('\n').Where(StringTools.NotNullOrWhiteSpace)
                .Select(ParseEncodedTreeDataLine)
                .Single();

            yield return Answer1(data);
            yield return Answer2(data);
        }


        public static int Answer1(IImmutableList<int> data)
        {
            var tree = ParseTree(data);

            var sum = tree.TraverseDepthFirst(0, (acc, node) => acc + node.Metadata.Sum());

            return sum;
        }

        public static int Answer2(IImmutableList<int> data)
        {
            var tree = ParseTree(data);

            int Visitor(TreeNode node)
            {
                if (node.Children.IsEmpty())
                {
                    return node.Metadata.Sum();
                }
                else
                {
                    return node.Metadata
                        .Select(m =>
                        {
                            if (m == 0) { return 0; }
                            else if (m > node.Children.Count) { return 0; }
                            else { return Visitor(node.Children[m - 1]); }
                        })
                        .Sum();
                }
            }

            var sum = Visitor(tree);

            return sum;
        }

        public static IImmutableList<int> ParseEncodedTreeDataLine(string line)
        {
            return line
                    .Split(' ', '\t', '\r')
                    .Where(part => part.NotNullOrEmpty())
                    .Select(int.Parse)
                    .ToImmutableArray();
        }

        public static TreeNode ParseTree(IImmutableList<int> encoded)
        {
            var unprocessed = new Stack<int>(encoded.Reverse());

            return Parse(new TreeNode());

            TreeNode Parse(TreeNode current)
            {
                var nChildren = unprocessed.Pop();
                var nMetadata = unprocessed.Pop();
                for (int i = 0; i < nChildren; i++)
                {
                    current.Children.Add(Parse(new TreeNode()));
                }
                for (int i = 0; i < nMetadata; i++)
                {
                    current.Metadata.Add(unprocessed.Pop());
                }

                return current;
            }
        }
    }
}
