﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Kodfodrasz.AoC.Year2018.Day8
{
    public class TreeNode
    {
        public TreeNode()
        {
        }

        public TreeNode(List<TreeNode> children, List<int> metadata)
        {
            Children = children;
            Metadata = metadata;
        }

        public List<TreeNode> Children { get; set; } = new List<TreeNode>();
        public List<int> Metadata { get; set; } = new List<int>();

        public T TraverseDepthFirst<T>(T accumulator, Func<T,TreeNode,T> visitor)
        {
            T visitResult = visitor(accumulator, this);
            foreach(var child in Children)
            {
                visitResult = child.TraverseDepthFirst(visitResult, visitor);
            }
            return visitResult;
        }
    }
}
