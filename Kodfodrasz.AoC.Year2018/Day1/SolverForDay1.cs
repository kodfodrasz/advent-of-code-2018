﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day1 {
    public class SolverForDay1 : SolverBase {
        public SolverForDay1() : base("Chronal Calibration") { }

        public override IEnumerable<object> Solve(string input) {
            var data = input.Split('\n').Where(s => !string.IsNullOrWhiteSpace(s)).Select(int.Parse).ToImmutableArray();

            yield return Answer1(data);
            yield return Answer2(data);
        }

        private static int Answer1(ImmutableArray<int> data) {
            return data.Sum();
        }

        private static object Answer2(ImmutableArray<int> data) {
            int sum = 0;
            var set = new HashSet<int> { sum };

            foreach (var d in data.Repeat().SelectMany(x => x)) {
                sum += d;
                if (set.TryGetValue(sum, out var _)) {
                    break;
                } else {
                    set.Add(sum);
                }
            }

            return sum;
        }
    }
}
