﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day4 {
    public class SolverForDay4 : SolverBase {
        public SolverForDay4() : base("Repose Record") { }

        public override IEnumerable<object> Solve(string input) {
            var data = input.Split('\n').Where(s => !string.IsNullOrWhiteSpace(s)).Select(ParseLogLine).ToImmutableArray();

            yield return Answer1(data);
            yield return Answer2(data);
        }

        private static object Answer1(IImmutableList<(DateTime timestamp, string text)> data) {
            var cards = (from day in ParseWorkdays(data)
                         group day by day.guard into grp
                         select (guard: grp.Key, days: grp.Select(g => g.timecard).ToImmutableArray())
                        ).ToImmutableArray();

            var topSleeper = cards.Select(c => (
                c.guard,
                slept: c.days.SelectMany(_ => _).Where(x => x == Asleep).Count()
            ))
            .OrderByDescending(g => g.slept)
            .First().guard;

            var topSleeperCards = cards.Where(c => c.guard == topSleeper).Single().days;

            var sleepmin = Enumerable.Range(0, 60)
                .Select(i => topSleeperCards.Select(c => c[i]).Count(x => x == Asleep))
                .ToImmutableArray();

            int minute = sleepmin.IndexOf(sleepmin.Max());

            return topSleeper * minute;
        }


        private static object Answer2(IImmutableList<(DateTime timestamp, string text)> data) {
            var cards = (from day in ParseWorkdays(data)
                         group day by day.guard into grp
                         select (guard: grp.Key, days: grp.Select(g => g.timecard).ToImmutableArray())
                        ).ToImmutableArray();

            var topsleeper2 = cards.Select(card => {
                var sleepmin = Enumerable.Range(0, 60)
                    .Select(i => card.days.Select(c => c[i]).Count(x => x == Asleep))
                    .ToImmutableArray();

                int minute = sleepmin.IndexOf(sleepmin.Max());

                return (card.guard, minute, num: sleepmin.Max());
            })
            .OrderByDescending(x => x.num)
            .First();

            return topsleeper2.guard * topsleeper2.minute;
        }

        const char Awake = '.';
        const char Asleep = '#';

        public static IEnumerable<(int guard, char[] timecard)>
        ParseWorkdays(IImmutableList<(DateTime timestamp, string text)> data) {
            Regex regex = new Regex(@"Guard #(\d+) begins shift");

            char[] timecard = Awake.Repeat(60).ToArray();

            int guard = -1;
            DateTime? sleepStart = null;
            foreach (var d in data.OrderBy(d => d.timestamp)) {
                var gmatch = regex.Match(d.text);
                if (gmatch.Success) {
                    if (sleepStart != null) {
                        throw new Exception("Sleeping at shift");
                    }
                    if (guard != -1) {
                        yield return (guard, timecard);
                    }
                    guard = gmatch.Groups[1].Value.ParseInt();
                    timecard = Awake.Repeat(60).ToArray();
                } else if (d.text == "falls asleep") {
                    sleepStart = d.timestamp;
                } else if (d.text == "wakes up") {
                    AddSleep(sleepStart.Value, d.timestamp, timecard);
                    sleepStart = null;
                }
            }
            yield return (guard, timecard);


            void AddSleep(DateTime sleep, DateTime wake, char[] record) {
                if (sleep.Hour > 0) {
                    // skip to 00:00 next day
                    sleep = sleep.Date.AddDays(1);
                }

                for (int m = sleep.Minute; m < wake.Minute; m++) {
                    record[m] = Asleep;
                }
            }
        }

        // [1518-11-01 00:00] Guard #10 begins shift
        private static readonly Regex regex = new Regex(@"\[(\d\d\d\d-\d\d-\d\d \d\d:\d\d)\] (.*)");

        public static (DateTime timestamp, string text) ParseLogLine(string line) {
            var match = regex.Match(line);
            if (!match.Success) {
                throw new ArgumentOutOfRangeException($"Not a valid input line: {line}");
            }

            return (
                timestamp: match.Groups[1].Value.ParseDateTime(),
                text: match.Groups[2].Value
            );
        }
    }
}
