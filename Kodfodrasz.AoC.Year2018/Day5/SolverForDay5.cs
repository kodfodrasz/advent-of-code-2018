﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day5 {
    public class SolverForDay5 : SolverBase {
        public SolverForDay5() : base("Alchemical Reduction") { }

        public override IEnumerable<object> Solve(string input) {
            var data = input.Split('\n').Where(StringTools.NotNullOrWhiteSpace).Single();

            yield return Answer1(data);
            yield return Answer2(data);
        }

        public static string Annihilate(string data) {
            const char skipChar = '.';

            if (data.Any(c => c == skipChar)) {
                throw new Exception("Skipped character marker exists in input!");
            }

            var d = data.ToArray();
            bool skipped = true;
            while (skipped) {
                skipped = false;
                for (int i = 0; i < d.Length - 1; i++) {
                    char a = d[i];
                    char b = d[i + 1];

                    if (IsReversePolarity(a, b)) {
                        d[i] = skipChar;
                        d[i + 1] = skipChar;
                        skipped = true;
                    }
                }
                d = d.Where(c => c != skipChar).ToArray();
            }

            return d.AsString();
        }

        public static object Answer1(string data) {
            var res = Annihilate(data);

            return res.Length;
        }

        private static object Answer2(string data) {
            var units = data.ToLowerInvariant().Distinct().OrderBy(c => c).ToArray();

            var processed = units.AsParallel().Select(u => {
                var U = char.ToUpper(u);
                var d = data.Where(c => !(c == u || c == U)).ToArray().AsString();

                var res = Annihilate(d);

                return (unit: u, res);
            })
            .ToArray();

            var shortest = processed.OrderBy(r => r.res.Length)
                .First();

            return shortest.res.Length;
        }

        public static bool IsReversePolarity(char a, char b)
            => IsSameChar(a, b) && IsDifferentCase(a, b);

        private static bool IsDifferentCase(char a, char b) => (char.IsUpper(a) && char.IsLower(b)) || (char.IsLower(a) && char.IsUpper(b));
        private static bool IsSameChar(char a, char b) => char.ToUpperInvariant(a) == char.ToUpperInvariant(b);
    }
}
