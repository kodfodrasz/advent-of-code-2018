﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day3 {
    public class SolverForDay3 : SolverBase {
        public SolverForDay3() : base("No Matter How You Slice It") { }

        public override IEnumerable<object> Solve(string input) {
            var data = input.Split('\n').Where(s => !string.IsNullOrWhiteSpace(s)).Select(ParseClaim).ToImmutableArray();

            yield return Answer1(data);
            yield return Answer2(data);
        }

        private static object Answer1(ImmutableArray<(int id, int left, int top, int width, int height)> data) {
            // index: [top, left], 1000x1000
            var canvas = new int[1001, 1001];

            foreach (var claim in data) {
                for (int left = claim.left; left < claim.left + claim.width; left++) {
                    for (int top = claim.top; top < claim.top + claim.height; top++) {
                        canvas[top, left]++;
                    }
                }
            }

            return canvas.EnumerateByRows().Where(i => i >= 2).Count();
        }

        private static object Answer2(ImmutableArray<(int id, int left, int top, int width, int height)> data) {
            var canvas = new HashSet<int>[1001, 1001];

            foreach (var claim in data) {
                for (int left = claim.left; left < claim.left + claim.width; left++) {
                    for (int top = claim.top; top < claim.top + claim.height; top++) {
                        if (canvas[top, left] == null) {
                            canvas[top, left] = new HashSet<int>();
                        }
                        canvas[top, left].Add(claim.id);
                    }
                }
            }

            var singleIds = data.Select(d => d.id).ToHashSet();

            foreach (var cell in canvas.EnumerateByRows()) {
                if ((cell?.Count ?? 0) > 1) {
                    foreach (var id in cell) {
                        singleIds.Remove(id);
                    }
                }
            }

            return singleIds.Single();
        }

        // #1 @ 1,3: 4x4
        private static readonly Regex regex = new Regex(@"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)");

        public static
        (int id, int left, int top, int width, int height)
        ParseClaim(string line) {
            var match = regex.Match(line);
            if (!match.Success) {
                throw new ArgumentOutOfRangeException($"Not a valid input line: {line}");
            }

            return (
                id: match.Groups[1].Value.ParseInt(),
                left: match.Groups[2].Value.ParseInt(),
                top: match.Groups[3].Value.ParseInt(),
                width: match.Groups[4].Value.ParseInt(),
                height: match.Groups[5].Value.ParseInt()
            );
        }
    }
}
