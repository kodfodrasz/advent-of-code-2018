﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day6
{
    public class SolverForDay6 : SolverBase
    {
        public SolverForDay6() : base("Chronal Coordinates") { }

        public override IEnumerable<object> Solve(string input)
        {
            var data = input.Split('\n').Where(StringTools.NotNullOrWhiteSpace)
                .Select(ParseCoordinate)
                .ToImmutableArray();

            yield return Answer1(data);
            yield return Answer2(data);
        }


        public static object Answer1(IImmutableList<(int x, int y)> data)
        {
            const int MultipleNeighbours = -1;
            var marked = new ConcurrentDictionary<(int x, int y), int>();

            // Distance 0;
            for (int i = 0; i < data.Count; i++)
            {
                marked.TryAdd(data[i], i);
            }

            int distance = 0;
            int targetdistance = Math.Max(10,
                                (from c1 in data
                                 from c2 in data
                                 select Coordinates.ManhattanDistance(c1, c2))
                                 .Max() / 2);
            int finitesUnchangedIterations = 0;
            int minFinitesUnchangedIterations = 5;

            ImmutableSortedSet<int> finites = ImmutableSortedSet<int>.Empty;

            while (OnlyInfinitesLeft())
            {
#if DEBUG
                var step = DebugFormat(distance, marked, Alphabet);
#endif

                var frontier = marked.Keys
                    .SelectMany(Neighbours4)
                    .Where(coord => !marked.ContainsKey(coord))
                    .Distinct()
                    .ToImmutableArray();
                var marks = frontier.Select(CalculateMarkByNeighbour).ToImmutableArray();

                if (marks.Length != frontier.Length) { throw new Exception("This should not happen!"); }

                foreach (var (coord, mark) in frontier.Zip(marks, (f, m) => (coord: f, mark: m)))
                {
                    if (marked.TryAdd(coord, mark).IsFalse()) { throw new Exception("Attempted to mark an already marked cell!"); }
                }

                var currentFinites = Enumerable.Range(0, data.Count).Except(marks).ToImmutableSortedSet();

                if (finites.SymmetricExcept(currentFinites).IsEmpty)
                {
                    finitesUnchangedIterations++;
                }
                else
                {
                    finites = currentFinites;
                    finitesUnchangedIterations = 0;
                }

                distance++;
            }

            var finiteAreas = marked.Values
                .Where(v => finites.Contains(v))
                .GroupBy(_ => _)
                .Select(grp => (mark: grp.Key, area: grp.Count()))
                .OrderByDescending(x => x.area)
                .ToArray();

            return finiteAreas.First().area;

            bool OnlyInfinitesLeft() => distance < targetdistance || finitesUnchangedIterations < minFinitesUnchangedIterations;

            int CalculateMarkByNeighbour((int x, int y) coord)
            {
                var neighbours = Neighbours4(coord)
                    .Select(neighbour =>
                    {
                        if (marked.TryGetValue(neighbour, out var fill)) { return fill; }
                        else { return int.MinValue; }
                    })
                    .Where(fill => fill != int.MinValue)
                    .Distinct()
                    .ToImmutableArray();

                switch (neighbours.Length)
                {
                    case 0: throw new Exception("Cannot mark neighbours, fill ha not reached neighbour cells yet!");
                    case 1: return neighbours.First();
                    default: return MultipleNeighbours;
                }
            }
        }

        public static object Answer2(IImmutableList<(int x, int y)> data)
        {
            const int DistanceLimit = 10000;
            return Answer2Calculator(data, DistanceLimit);
        }

        public static object Answer2Calculator(IImmutableList<(int x, int y)> data, int DistanceLimit)
        {
            var marked = new ConcurrentDictionary<(int x, int y), int>();

            var centerOfGravity = (
                x: (int)data.Select(coord => coord.x).Average().Round(),
                y: (int)data.Select(coord => coord.y).Average().Round()
            );
            var centerDistance = data.Select(coord => coord.ManhattanDistance(centerOfGravity)).Sum();

            marked.TryAdd(centerOfGravity, centerDistance);

            int lastMarkedCount = 1;
            while (0 < lastMarkedCount)
            {
                var frontier = marked.Keys
                                .SelectMany(Neighbours4)
                                .Distinct()
                                .Where(coord => !marked.ContainsKey(coord))
                                .ToImmutableArray();

                var distances = frontier.Select(coord => data.Select(dataCoord => coord.ManhattanDistance(dataCoord)).Sum());

                var newMarks = frontier.Zip(distances, (f, d) => (coord: f, distance: d))
                    .Where(x => x.distance < DistanceLimit)
                    .ToImmutableArray();

                foreach (var (coord, distance) in newMarks)
                {
                    if (marked.TryAdd(coord, distance).IsFalse()) { throw new Exception("Attempted to mark an already marked cell!"); }
                }

                lastMarkedCount = newMarks.Length;
            }

            return marked.Count;
        }

        public static IEnumerable<(int x, int y)> Neighbours4((int x, int y) coord)
        {
            yield return (coord.x - 1, coord.y);
            yield return (coord.x, coord.y + 1);
            yield return (coord.x + 1, coord.y);
            yield return (coord.x, coord.y - 1);
        }

        readonly static Regex coordinateLineRegex = new Regex(@"^\s*(\d+)\s*,\s*(\d+)\s*$");
        public static (int x, int y) ParseCoordinate(string line)
        {
            var match = coordinateLineRegex.Match(line);
            if (!match.Success)
            {
                throw new Exception($"Could not parse line to coordinates: {line}");
            }

            return (
                x: match.Groups[1].Value.ParseInt(),
                y: match.Groups[2].Value.ParseInt()
            );
        }

        public static string DebugFormat(int distance, ConcurrentDictionary<(int x, int y), int> marked, IList<char> symbols)
        {
            var minX = marked.Keys.Select(c => c.x).Min();
            var maxX = marked.Keys.Select(c => c.x).Max();
            var minY = marked.Keys.Select(c => c.y).Min();
            var maxY = marked.Keys.Select(c => c.y).Max();

            var sb = new StringBuilder();

            sb.AppendLine($"Distance: {distance}");
            sb.AppendLine();

            for (int y = minY; y <= maxY; y++)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    if (marked.TryGetValue((x, y), out var mark))
                    {
                        if (mark == -1) { sb.Append('.'); }
                        else
                        {
                            if (mark < symbols.Count)
                            {
                                sb.Append(symbols[mark]);
                            }
                            else
                            {
                                sb.Append('*');
                            }
                        }
                    }
                    else
                    {
                        sb.Append(' ');
                    }
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

        static IEnumerable<char> BuildAlphabet()
        {
            for (char ch = 'a'; ch < 'z'; ch++)
            {
                yield return ch;
            }
            for (char ch = 'A'; ch < 'Z'; ch++)
            {
                yield return ch;
            }
        }
        static readonly ImmutableArray<char> Alphabet = BuildAlphabet().ToImmutableArray();
    }
}
