﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day11
{
    public class SolverForDay11 : SolverBase
    {
        public SolverForDay11() : base("Chronal Charge") { }

        public override IEnumerable<object> Solve(string input)
        {
            var data = input.Split('\n').Where(StringTools.NotNullOrWhiteSpace)
                .Select(int.Parse)
                .Single();

            yield return Answer1(data);
            yield return Answer2(data);
        }

        public static int CalculateGridCell(int serial, int x, int y)
        {
            var rackId = x + 10;
            var powerLevel = rackId * y;
            powerLevel += serial;
            powerLevel *= rackId;

            var hundreds = (powerLevel / 100) % 10;

            return hundreds - 5;
        }

        public static int[,] CalculateGrid(int serial)
        {
            var grid = (int[,])Array.CreateInstance(
                elementType: typeof(int),
                lengths: new[] { 300, 300 },
                lowerBounds: new[] { 1, 1 }
            );

            for (int x = grid.GetLowerBound(0); x <= grid.GetUpperBound(0); x++)
            {
                for (int y = grid.GetLowerBound(1); y <= grid.GetUpperBound(1); y++)
                {
                    grid[x, y] = CalculateGridCell(serial, x, y);
                }
            }

            return grid;
        }

        public static int[,] RunSumKernel(int[,] grid, int kernelSize)
        {
            var summed = (int[,])Array.CreateInstance(
                            elementType: typeof(int),
                            lengths: new[] { grid.GetLength(0) - (kernelSize - 1), grid.GetLength(1) - (kernelSize - 1) },
                            lowerBounds: new[] { grid.GetLowerBound(0), grid.GetLowerBound(1) }
                    );

            for (int x = summed.GetLowerBound(0); x <= summed.GetUpperBound(0); x++)
            {
                for (int y = summed.GetLowerBound(1); y <= summed.GetUpperBound(1); y++)
                {
                    summed[x, y] = (from xx in Enumerable.Range(x, kernelSize)
                                    from yy in Enumerable.Range(y, kernelSize)
                                    select grid[xx, yy]
                                  ).Sum();
                }
            }

            return summed;
        }

        public static (int x, int y, int value) FindMaxIndex(int[,] grid)
        {
            var query = from x in Enumerable.Range(grid.GetLowerBound(0), grid.GetLength(0))
                        from y in Enumerable.Range(grid.GetLowerBound(1), grid.GetLength(1))
                        let value = grid[x, y]
                        orderby value descending
                        select (x, y, value);

            return query.First();
        }

        public static (int x, int y, int value) Answer1Extended(int data)
        {
            var grid = CalculateGrid(data);
            var summed = RunSumKernel(grid, 3);

            var result = FindMaxIndex(summed);

            return result;
        }

        public static string Answer1(int data)
        {
            var (x, y, value) = Answer1Extended(data);

            return $"{x},{y}";
        }

        public static async Task<(int x, int y, int value, int kernelSize)> Answer2Extended(int data)
        {
            var grid = CalculateGrid(data);

            var sums = await Enumerable.Range(1, 300)
                .Select(kernelSize => Task.Run(() =>
                {
                    var summed = RunSumKernel(grid, kernelSize);
                    var (x, y, value) = FindMaxIndex(summed);
                    return (x, y, value, kernelSize);
                }))
                .WhenAll();


            var query = sums.OrderByDescending(max => max.value);

            return query.First();
        }

        public static string Answer2(int data)
        {
            var (x, y, value, kernelSize) = Answer2Extended(data).ConfigureAwait(false).GetAwaiter().GetResult();

            return $"{x},{y},{kernelSize}";
        }
    }
}
