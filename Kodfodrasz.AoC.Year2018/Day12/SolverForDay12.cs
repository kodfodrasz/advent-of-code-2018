﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day12
{
    public class SolverForDay12 : SolverBase
    {
        public SolverForDay12() : base("Subterranean Sustainability") { }

        public override IEnumerable<object> Solve(string input)
        {
            var lines = input.Split('\n', '\r')
                .Where(StringTools.NotNullOrWhiteSpace)
                .Select(s => s.Trim());

            ImmutableSortedSet<int> initialState = ParseInitialState(lines);
            ImmutableDictionary<string, char> rules = ParseRules(lines);

            yield return Answer1(initialState, rules);
            yield return Answer2(initialState, rules);
        }


        public static int Answer1(ImmutableSortedSet<int> initialState, ImmutableDictionary<string, char> rules)
        {
            var state = initialState;
            for (int i=0;i<20; i++)
            {
                state = Iterate(state, rules);
            }

            return state.Sum();
        }

        private static object Answer2(ImmutableSortedSet<int> initialState, ImmutableDictionary<string, char> rules)
        {
            return "TODO";
        }

        public static ImmutableSortedSet<int> Iterate(ImmutableSortedSet<int> state, ImmutableDictionary<string, char> rules)
        {
            var low = state.Min() - 2;
            var count = state.Max() - state.Min() + 4;

            var query = from i in Enumerable.Range(low, count)
                        let neighbours = Enumerable.Range(i - 2, 5).Select(x => state.Contains(x) ? OccupiedCell : EmptyCell).ToArray().AsString()
                        select (number: i, state: rules.GetValueOrDefault(neighbours, EmptyCell));

            return query
                .Where(res => res.state == OccupiedCell)
                .Select(res => res.number)
                .ToImmutableSortedSet();
        }

        private static readonly char EmptyCell = '.';
        private static readonly char OccupiedCell = '#';

        private static readonly Regex InitialStateRegex = new Regex(@"^initial state:\s*([#.]+)$");
        public static ImmutableSortedSet<int> ParseInitialState(string line)
        {
            var match = InitialStateRegex.Match(line);
            if (!match.Success)
            {
                throw new Exception($"Could not parse line to assembly steps: {line}");
            }

            var initalStateString = match.Groups[1].Value;

            var setBuilder = ImmutableSortedSet<int>.Empty.ToBuilder();

            for (int i = 0; i < initalStateString.Length; i++)
            {
                if (initalStateString[i] == OccupiedCell) { setBuilder.Add(i); }
            }

            return setBuilder.ToImmutableSortedSet();
        }

        private static readonly Regex RuleRegex = new Regex(@"^([.#]{5})\s*=>\s*([.#])$");
        public static (string pattern, char result) ParseRule(string line)
        {
            var match = RuleRegex.Match(line);
            if (!match.Success)
            {
                throw new Exception($"Could not parse line to assembly steps: {line}");
            }

            return (
                pattern: match.Groups[1].Value,
                result: match.Groups[2].Value.Single()
            );
        }

        public static ImmutableDictionary<string, char> ParseRules(IEnumerable<string> lines)
        {
            return lines
                            .Skip(1)
                            .Select(ParseRule)
                            .ToImmutableDictionary(
                                keySelector: rule => rule.pattern,
                                elementSelector: rule => rule.result
                            );
        }

        public static ImmutableSortedSet<int> ParseInitialState(IEnumerable<string> lines)
        {
            return lines
                                .Take(1)
                                .Select(ParseInitialState)
                                .Single();
        }
    }
}
