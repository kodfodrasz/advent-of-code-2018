﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day7
{
    public class SolverForDay7 : SolverBase
    {
        public SolverForDay7() : base("The Sum of Its Parts") { }

        public override IEnumerable<object> Solve(string input)
        {
            var data = input.Split('\n').Where(StringTools.NotNullOrWhiteSpace)
                .Select(ParseAssemblyStep)
                .ToImmutableArray();

            yield return Answer1(data);
            yield return Answer2(data);
        }


        public static object Answer1(IImmutableList<(char step, char prerequisite)> data)
        {
            return "TODO";
        }

        public static object Answer2(IImmutableList<(char step, char prerequisite)> data)
        {
            return "TODO";
        }

        readonly static Regex assmelyStepLineRegex = new Regex(@"^\s*Step (\w) must be finished before step (\w) can begin\.\s*$");
        public static (char step, char prerequisite) ParseAssemblyStep(string line)
        {
            var match = assmelyStepLineRegex.Match(line);
            if (!match.Success)
            {
                throw new Exception($"Could not parse line to assembly steps: {line}");
            }

            return (
                step: match.Groups[1].Value.Single(),
                prerequisite: match.Groups[2].Value.Single()
            );
        }
    }

    static class Day7SyntaxSugar
    {
        public static void Add<T>(this IList<T> list, params T[] items)
        {
            foreach (var i in items) { list.Add(i); }
        }

        public static void Add<T>(this IList<T> list, IEnumerable<T> items)
        {
            foreach (var i in items) { list.Add(i); }
        }

        public static IEnumerable<char> AllSteps(this ImmutableSortedDictionary<char, ImmutableArray<char>> dependencyGraph)
            => dependencyGraph.Keys.Distinct().OrderBy();

        public static IEnumerable<char> AllPrerequisites(this ImmutableSortedDictionary<char, ImmutableArray<char>> dependencyGraph)
            => dependencyGraph.Values.SelectMany(_ => _).Distinct().OrderBy();

        public static IEnumerable<char> StepPrerequisites(this ImmutableSortedDictionary<char, ImmutableArray<char>> dependencyGraph, char stepId)
            => dependencyGraph[stepId];


    }
}
