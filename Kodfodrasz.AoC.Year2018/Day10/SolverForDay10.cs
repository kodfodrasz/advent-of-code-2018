﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Year2018.Day10
{
    public class SolverForDay10 : SolverBase
    {
        public SolverForDay10() : base("The Stars Align") { }

        public override IEnumerable<object> Solve(string input)
        {
            var data = input.Split('\n', '\r')
                    .Where(StringTools.NotNullOrWhiteSpace)
                    .Select(s => s.Trim())
                    .Select(ParseLine)
                    .ToImmutableArray();

            yield return Answer1(data);
            yield return Answer2(data);
        }

        public static object Answer1(ImmutableArray<((int x, int y) position, (int x, int y) velocity)> data)
        {
            var previous = data;
            var current = data;

            do
            {
                previous = current;
                current = Iterate(previous);
            } while (Area(previous) > Area(current));

            return Render(previous, emptyChar: ' ');
        }

        public static object Answer2(ImmutableArray<((int x, int y) position, (int x, int y) velocity)> data)
        {
            var previous = data;
            var current = data;

            int i = 0;
            do
            {
                previous = current;
                current = Iterate(previous);
                i++;
            } while (Area(previous) > Area(current));

            return i - 1;
        }


        readonly static Regex LineRegex = new Regex(@"^position=<\s*(-?\d+),\s*(-?\d+)>\s*velocity=<\s*(-?\d+),\s*(-?\d+)>$");

        public static ((int x, int y) position, (int x, int y) velocity) ParseLine(string line)
        {
            var match = LineRegex.Match(line);
            if (!match.Success)
            {
                throw new Exception($"Could not parse line to position and velocitys: {line}");
            }

            return (
                position: (
                    x: match.Groups[1].Value.ParseInt(),
                    y: match.Groups[2].Value.ParseInt()
                ),
                velocity: (
                    x: match.Groups[3].Value.ParseInt(),
                    y: match.Groups[4].Value.ParseInt()
                )
            );
        }

        public static string Render(ImmutableArray<((int x, int y) position, (int x, int y) velocity)> data, char filledChar = '#', char emptyChar = '.')
        {
            var sb = new StringBuilder();

            int minX = data.Select(d => d.position.x).Min();
            int maxX = data.Select(d => d.position.x).Max();
            int minY = data.Select(d => d.position.y).Min();
            int maxY = data.Select(d => d.position.y).Max();

            var coordinates = data.Select(d => d.position).ToImmutableHashSet();

            (int, int) ToCoordinate(int xIndex, int yIndex)
                => (minX + xIndex, minY + yIndex);

            sb.AppendLine();
            for (int y = 0; y <= (maxY - minY); y++)
            {
                for (int x = 0; x <= (maxX - minX); x++)
                {
                    if (coordinates.Contains(ToCoordinate(x, y)))
                    {
                        sb.Append(filledChar);
                    }
                    else
                    {
                        sb.Append(emptyChar);
                    }
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

        public static ImmutableArray<((int x, int y) position, (int x, int y) velocity)>
            Iterate(ImmutableArray<((int x, int y) position, (int x, int y) velocity)> data)
        {
            return data.Select(d => (
                    position: (
                        x: d.position.x + d.velocity.x,
                        y: d.position.y + d.velocity.y
                    ),
                    d.velocity
            ))
            .ToImmutableArray();
        }

        public static double Area(ImmutableArray<((int x, int y) position, (int x, int y) velocity)> data)
        {

            double minX = data.Select(d => d.position.x).Min();
            double maxX = data.Select(d => d.position.x).Max();
            double minY = data.Select(d => d.position.y).Min();
            double maxY = data.Select(d => d.position.y).Max();

            return (maxX - minX) * (maxY - minY);
        }
    }
}
