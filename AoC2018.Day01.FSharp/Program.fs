﻿open System
open Kodfodrasz.AoC
open System.Collections.Generic

let data = 
    Input.GetInput(2018, 1)
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> fun s -> s.Split('\n')
    |> Seq.where(fun s -> not(String.IsNullOrWhiteSpace(s)))
    |> Seq.map(LanguagePrimitives.ParseInt32)
    |> Seq.toArray

let answer1 = data |> Seq.sum

let infinite seq' = 
    seq { while true do yield! seq' }

let answer2 = 
    let sums = data 
                |> infinite
                |> Seq.scan (+) 0

    let seen = HashSet()
    sums |> Seq.find(fun s -> not(seen.Add(s)))


[<EntryPoint>]
let main argv =

    printfn "Answer 1: %d" answer1
    printfn "Answer 2: %d" answer2

    if System.Diagnostics.Debugger.IsAttached then
        printfn ""
        printfn "Press any key to exit..."
        Console.ReadKey() |> ignore

    0 // return an integer exit code
