﻿using System;
using System.Collections.Immutable;
using FluentAssertions;
using Xunit;

namespace Kodfodrasz.AoC.Tests
{
    public class TransposeTests
    {
        [Fact]
        public void Transpose_Array2D()
        {
            var input = new int[,]
            {
                { 1, 2 },
                { 3, 4 },
                { 5, 6 }
            };

            var result = input.Transpose();
            result.GetLength(0).Should().Be(2);
            result.GetLength(1).Should().Be(3);

            result[0, 0].Should().Be(1);
            result[0, 1].Should().Be(3);
            result[0, 2].Should().Be(5);
            result[1, 0].Should().Be(2);
            result[1, 1].Should().Be(4);
            result[1, 2].Should().Be(6);
        }

        [Fact]
        public void Transpose_JaggedArray()
        {
            var input = new int[][]
            {
                new[]{ 1, 2 },
                new[]{ 3, 4 },
                new[]{ 5, 6 }
            };

            var result = input.Transpose();
            result.Length.Should().Be(2);
            result[0].Length.Should().Be(3);
            result[1].Length.Should().Be(3);

            result[0][0].Should().Be(1);
            result[0][1].Should().Be(3);
            result[0][2].Should().Be(5);
            result[1][0].Should().Be(2);
            result[1][1].Should().Be(4);
            result[1][2].Should().Be(6);
        }
    }
}
