﻿using System;
using System.Collections.Immutable;
using FluentAssertions;
using Xunit;

namespace Kodfodrasz.AoC.Tests {
    public class EnumerationTests {
        [Fact]
        public void EnumerateByRows_Array2D() {
            var input = new int[,]
            {
                { 1, 2 },
                { 3, 4 },
                { 5, 6 }
            };

            var result = input.EnumerateByRows();

            result.Should().BeEquivalentTo(new[] { 1, 2, 3, 4, 5, 6 });
        }

        [Fact]
        public void EnumerateByColumns_Array2D() {
            var input = new int[,]
            {
                { 1, 4 },
                { 2, 5 },
                { 3, 6 }
            };

            var result = input.EnumerateByRows();

            result.Should().BeEquivalentTo(new[] { 1, 2, 3, 4, 5, 6 });
        }

        [Fact]
        public void EnumerateZigZag_Array2D_Column() {
            var input = new int[,]
            {
                { 1, 3 },
                { 2, 5 },
                { 4, 6 },
                { 7, 8 }
            };

            var result = input.EnumerateDiagonally();

            result.Should().BeEquivalentTo(new[] { 1, 2, 3, 4, 5, 6, 7, 8 });
        }

        [Fact]
        public void EnumerateZigZag_Array2D_Row() {
            var input = new int[,]
            {
                { 1, 3, 5, 7 },
                { 2, 4, 6, 8 },
            };

            var result = input.EnumerateDiagonally();

            result.Should().BeEquivalentTo(new[] { 1, 2, 3, 4, 5, 6, 7, 8 });
        }
    }
}
