using System;
using FluentAssertions;
using Xunit;

namespace Kodfodrasz.AoC.Tests
{
    public class HammingDistanceTests
    {
        [Fact]
        public void HammingDistanceThrowsForNullInputA()
        {
            Action call = () => StringTools.HammingDistance(null, "b");

            call.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void HammingDistanceThrowsForNullInputB()
        {
            Action call = () => StringTools.HammingDistance("a", null);

            call.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void HammingDistanceThrowsForDifferentInputLength()
        {
            Action call1 = () => StringTools.HammingDistance("a", "aaa");

            call1.Should().Throw<ArgumentOutOfRangeException>();

            Action call2 = () => StringTools.HammingDistance("", "a");

            call2.Should().Throw<ArgumentOutOfRangeException>();

            Action call3 = () => StringTools.HammingDistance("a", "");

            call3.Should().Throw<ArgumentOutOfRangeException>();

            Action call4 = () => StringTools.HammingDistance("aaa", "a");

            call4.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData("", "", 0)]
        [InlineData("abcd", "abcd", 0)]
        [InlineData("abcd", "abcD", 1)]
        [InlineData("abcd", "efgh", 4)]
        [InlineData("karolin", "kathrin", 3)]
        [InlineData("karolin", "kerstin", 3)]
        [InlineData("1011101", "1001001", 2)]
        [InlineData("2173896", "2233796", 3)]
        public void HammingDistanceForValidInput(string a, string b, int expectedDistance)
        {
            var distance = StringTools.HammingDistance(a, b);

            distance.Should().Be(expectedDistance);
        }
    }
}
