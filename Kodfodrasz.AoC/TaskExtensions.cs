﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Kodfodrasz.AoC
{
    public static class TaskExtensions {
        //
        // Summary:
        //     Creates a task that will complete when all of the System.Threading.Tasks.Task
        //     objects in an enumerable collection have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Returns:
        //     A task that represents the completion of all of the supplied tasks.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks collection contained a null task.
        public static Task WhenAll(this IEnumerable<Task> tasks) {
            return Task.WhenAll(tasks);
        }

        //
        // Summary:
        //     Creates a task that will complete when all of the System.Threading.Tasks.Task
        //     objects in an array have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Returns:
        //     A task that represents the completion of all of the supplied tasks.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks array contained a null task.
        public static Task WhenAll(this Task[] tasks) {
            return Task.WhenAll(tasks);
        }

        //
        // Summary:
        //     Creates a task that will complete when all of the System.Threading.Tasks.Task`1
        //     objects in an enumerable collection have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Type parameters:
        //   TResult:
        //     The type of the completed task.
        //
        // Returns:
        //     A task that represents the completion of all of the supplied tasks.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks collection contained a null task.
        public static Task<TResult[]> WhenAll<TResult>(this IEnumerable<Task<TResult>> tasks) {
            return Task.WhenAll(tasks);
        }

        //
        // Summary:
        //     Creates a task that will complete when all of the System.Threading.Tasks.Task`1
        //     objects in an array have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Type parameters:
        //   TResult:
        //     The type of the completed task.
        //
        // Returns:
        //     A task that represents the completion of all of the supplied tasks.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks array contained a null task.
        public static Task<TResult[]> WhenAll<TResult>(this Task<TResult>[] tasks) {
            return Task.WhenAll(tasks);
        }

        //
        // Summary:
        //     Creates a task that will complete when any of the supplied tasks have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Returns:
        //     A task that represents the completion of one of the supplied tasks. The return
        //     task's Result is the task that completed.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks array contained a null task, or was empty.
        public static Task<Task> WhenAny(this IEnumerable<Task> tasks) {
            return Task.WhenAny(tasks);
        }

        //
        // Summary:
        //     Creates a task that will complete when any of the supplied tasks have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Returns:
        //     A task that represents the completion of one of the supplied tasks. The return
        //     task's Result is the task that completed.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks array contained a null task, or was empty.
        public static Task<Task> WhenAny(this Task[] tasks) {
            return Task.WhenAny(tasks);
        }

        //
        // Summary:
        //     Creates a task that will complete when any of the supplied tasks have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Type parameters:
        //   TResult:
        //     The type of the completed task.
        //
        // Returns:
        //     A task that represents the completion of one of the supplied tasks. The return
        //     task's Result is the task that completed.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks array contained a null task, or was empty.
        public static Task<Task<TResult>> WhenAny<TResult>(this IEnumerable<Task<TResult>> tasks) {
            return Task.WhenAny(tasks);
        }

        //
        // Summary:
        //     Creates a task that will complete when any of the supplied tasks have completed.
        //
        // Parameters:
        //   tasks:
        //     The tasks to wait on for completion.
        //
        // Type parameters:
        //   TResult:
        //     The type of the completed task.
        //
        // Returns:
        //     A task that represents the completion of one of the supplied tasks. The return
        //     task's Result is the task that completed.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     The tasks argument was null.
        //
        //   T:System.ArgumentException:
        //     The tasks array contained a null task, or was empty.
        public static Task<Task<TResult>> WhenAny<TResult>(this Task<TResult>[] tasks) {
            return Task.WhenAny(tasks);
        }
    }
}
