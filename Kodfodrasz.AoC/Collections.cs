﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Kodfodrasz.AoC {
    public static class Collections {
        public static bool IsIn<T>(this T item, IEnumerable<T> collection)
            => collection.Contains(item);

        public static bool IsIn<T>(this T item, params T[] collection)
            => collection.Contains(item);

        public static bool NotIn<T>(this T item, IEnumerable<T> collection)
            => item.IsIn(collection).IsFalse();

        public static bool NotIn<T>(this T item, params T[] collection)
            => item.IsIn(collection).IsFalse();

        public static bool IsEmpty<T>(this IEnumerable<T> collection)
            => collection.Any().IsFalse();

        public static bool NotEmpty<T>(this IEnumerable<T> collection)
            => collection.Any();

        public static IOrderedEnumerable<T> OrderBy<T>(this IEnumerable<T> sequence)
            => sequence.OrderBy(_ => _);

        public static IOrderedEnumerable<T> OrderByDescending<T>(this IEnumerable<T> sequence)
            => sequence.OrderByDescending(_ => _);

        public static T[,] Transpose<T>(this T[,] input) {
            var rank0 = input.GetUpperBound(0) + 1;
            var rank1 = input.GetUpperBound(1) + 1;

            var target = new T[rank1, rank0];

            if (rank0 == 0 || rank1 == 1) { return target; }

            for (int i = 0; i < rank1; i++) {
                for (int j = 0; j < rank0; j++) {
                    target[i, j] = input[j, i];
                }
            }

            return target;
        }

        public static T[][] Transpose<T>(this IList<IList<T>> input) {
            if (input.GroupBy(i => i.Count).Count() != 1) { throw new Exception("Not all input rows have the same length"); }
            if (input.Count == 0) { return new T[0][]; }

            return Enumerable.Range(0, input.First().Count)
                .Select(i => input.Select(inner => inner[i]).ToArray())
                .ToArray();
        }

        public static T[,] RotateClockwise<T>(this T[,] input) {
            var rank0 = input.GetLength(0);
            var rank1 = input.GetLength(1);

            var target = new T[rank1, rank0];

            if (rank0 == 0 || rank1 == 1) { return target; }

            var bound0 = input.GetUpperBound(0);

            for (int i = 0; i < rank1; i++) {
                for (int j = 0; j < rank0; j++) {
                    target[i, j] = input[bound0 - j, i];
                }
            }

            return target;
        }

        public static IList<IList<T>> RotateClockwise<T>(this IList<IList<T>> input) {
            if (input.GroupBy(i => i.Count).Count() != 1) { throw new Exception("Not all input rows have the same length"); }
            if (input.Count == 0) { return new T[0][]; }

            var reverse = input.Reverse().ToArray();

            return Enumerable.Range(0, input.First().Count)
                .Select(i => reverse.Select(inner => inner[i]).ToArray())
                .ToArray();
        }

        public static T[,] RotateCounterClockwise<T>(this T[,] input) {
            var rank0 = input.GetLength(0);
            var rank1 = input.GetLength(1);

            var target = new T[rank1, rank0];

            if (rank0 == 0 || rank1 == 1) { return target; }

            var bound1 = input.GetUpperBound(1);

            for (int i = 0; i < rank1; i++) {
                for (int j = 0; j < rank0; j++) {
                    target[i, j] = input[j, bound1 - i];
                }
            }

            return target;
        }

        public static IList<IList<T>> RotateCounterClockwise<T>(this IList<IList<T>> input) {
            if (input.GroupBy(i => i.Count).Count() != 1) { throw new Exception("Not all input rows have the same length"); }
            if (input.Count == 0) { return new T[0][]; }

            var reverse = input.Reverse().ToArray();
            var bound1 = input.First().Count - 1;

            return Enumerable.Range(0, input.First().Count)
                .Select(i => input.Select(inner => inner[bound1 - i]).ToArray())
                .ToArray();
        }

        public static IEnumerable<T> EnumerateByRows<T>(this T[,] input) {
            var rank0 = input.GetLength(0);
            var rank1 = input.GetLength(1);

            for (int i = 0; i < rank0; i++) {
                for (int j = 0; j < rank1; j++) {
                    yield return input[i, j];
                }
            }
        }

        public static IEnumerable<T> EnumerateByColumns<T>(this T[,] input) {
            var rank0 = input.GetLength(0);
            var rank1 = input.GetLength(1);

            for (int j = 0; j < rank1; j++) {
                for (int i = 0; i < rank0; i++) {
                    yield return input[i, j];
                }
            }
        }

        public static IEnumerable<T> EnumerateDiagonally<T>(this T[,] input) {
            var rank0 = input.GetLength(0);
            var rank1 = input.GetLength(1);

            if (rank0 == 0 || rank1 == 0) { yield break; }

            var count = rank0 * rank1;
            int visited = 0;

            int i = 0, j = 0;
            var bound1 = input.GetUpperBound(1);
            while (visited < count) {
                yield return input[i, j];
                visited++;

                do { Step(); } while (visited < count && OutOfBounds());
            }

            void Step() {
                i--;
                j++;

                if (i < 0) { i = j; j = 0; }
            }

            bool OutOfBounds()
                => (i < 0 || j < 0 || rank0 <= i || rank1 <= j);
        }
    }
}
