﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kodfodrasz.AoC
{
    public static class Coordinates
    {
        public static int ManhattanDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        public static int ManhattanDistance(this (int x, int y) coord1, (int x, int y) coord2)
            => ManhattanDistance(coord1.x, coord1.y, coord2.x, coord2.y);
    }
}
