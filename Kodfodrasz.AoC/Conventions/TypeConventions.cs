﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Kodfodrasz.AoC.Conventions {
    public static class TypeConventions {
        public static int SolverYear(this Type t) {
            var yearPart = t.FullName.Split('.')
                            .First(p => p.Contains("year", StringComparison.InvariantCultureIgnoreCase));

            var match = Regex.Match(yearPart, @"Year(\d{4})");
            if (match.Success) {
                return match.Groups[1].Value.ParseInt();
            }

            throw new ArgumentException($"Could not determine year of Solver for type {t.FullName}");
        }

        public static int SolverDay(this Type t) {
            var dayPart = t.FullName.Split('.')
                            .First(p => p.Contains("day", StringComparison.InvariantCultureIgnoreCase));

            var match = Regex.Match(dayPart, @"Day(\d+)");
            if (match.Success) {
                return match.Groups[1].Value.ParseInt();
            }

            throw new ArgumentException($"Could not determine day of Solver for type {t.FullName}");
        }
    }
}
