﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kodfodrasz.AoC.Model {
    public interface ISolver {
        int Year { get; }
        int Day { get; }

        string Name { get; }

        IEnumerable<object> Solve(string input);
    }
}
