﻿using System;
using System.Collections.Generic;
using System.Text;
using Kodfodrasz.AoC.Conventions;

namespace Kodfodrasz.AoC.Model {
    public abstract class SolverBase : ISolver {
        public int Year => GetType().SolverYear();

        public int Day => GetType().SolverDay();

        public string Name { get; }

        public abstract IEnumerable<object> Solve(string input);

        protected SolverBase(string name) {
            Name = name;
        }
    }
}
