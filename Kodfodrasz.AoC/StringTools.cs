﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kodfodrasz.AoC
{
    public static class StringTools
    {
        public static int HammingDistance(this string a, string b)
        {
            if (a == null) { throw new ArgumentNullException(nameof(a)); }
            if (b == null) { throw new ArgumentNullException(nameof(b)); }
            if (a.Length != b.Length) { throw new ArgumentOutOfRangeException("strings are not of the same length"); }

            return a.Zip(b, (x, y) => x != y).Count(z => z);
        }

        public static int ParseInt(this string a) 
            => int.Parse(a);

        public static DateTime ParseDateTime(this string a)
            => DateTime.Parse(a);

        public static bool IsNullOrWhiteSpace(this string s)
            => string.IsNullOrWhiteSpace(s);

        public static bool IsNullOrEmpty(this string s)
            => string.IsNullOrEmpty(s);

        public static bool NotNullOrWhiteSpace(this string s)
            => !string.IsNullOrWhiteSpace(s);

        public static bool NotNullOrEmpty(this string s)
            => !string.IsNullOrEmpty(s);

        public static string Join<T>(this IEnumerable<T> items, string separator)
            => string.Join(separator, items);

        public static string AsString(this IEnumerable<char> characters)
            => new string(characters.ToArray());
    }
}
