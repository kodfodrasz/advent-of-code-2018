﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kodfodrasz.AoC
{
    public static class Misc
    {
        public static bool IsFalse(this bool value)
            => !value;

        public static IEnumerable<T> Repeat<T>(this T item, int count)
            => Enumerable.Repeat(item, count);

        public static IEnumerable<T> Repeat<T>(this T item)
        {
            while (true) yield return item;
        }

        public static double Round(this double x)
            => Math.Round(x);
    }
}
