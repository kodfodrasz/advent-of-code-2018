﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kodfodrasz.AoC
{
    public static class Input
    {
        public static async Task<string> GetInput(int year, int day)
        {
            string AOC_DATA_DIR;
            AOC_DATA_DIR = Environment.GetEnvironmentVariable(nameof(AOC_DATA_DIR))
                    ?? throw new Exception($"Environment variable {nameof(AOC_DATA_DIR)} is unset!");

            Directory.CreateDirectory(AOC_DATA_DIR);

            string AOC_SESSION_COOKIE;
            AOC_SESSION_COOKIE = Environment.GetEnvironmentVariable(nameof(AOC_SESSION_COOKIE))
                    ?? throw new Exception($"Environment variable {nameof(AOC_SESSION_COOKIE)} is unset!");

            var inputPath = Path.Join(AOC_DATA_DIR, $"input-{year}-{day}.txt");

            string input;
            if (File.Exists(inputPath))
            {
                input = File.ReadAllText(inputPath);
            }
            else
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Cookie", $"session={AOC_SESSION_COOKIE}");

                var url = $"https://adventofcode.com/{year}/day/{day}/input";

                input = await client.GetStringAsync(url);

                File.WriteAllText(inputPath, input);
            }

            return input;
        }
    }
}
