﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Kodfodrasz.AoC.Conventions;
using Kodfodrasz.AoC.Model;

namespace Kodfodrasz.AoC.Cli
{
    public class Program
    {

        public static async Task Main(string[] args)
        {
            PrintProgramBanner();

            try
            {

                var options = ReadOptions(args);

                using (var container = BuildContainer(options))
                {
                    var allSolvers = container.Resolve<IEnumerable<ISolver>>()
                                .OrderBy(s => s.Year)
                                .ThenBy(s => s.Day)
                                .ToArray();

                    if ("all".IsIn(args))
                    {
                        await Execute(allSolvers);
                    }
                    else if ("last".IsIn(args))
                    {
                        var solver = allSolvers.Last();
                        await Execute(solver);
                    }
                    else
                    {
                        var solvers = args.Select(arg => container.ResolveNamed<ISolver>(arg))
                            .ToArray();

                        await Execute(solvers);
                    }

                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        Console.WriteLine();
                        Console.WriteLine("Press any key to exit...");
                        Console.ReadKey();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine();
                using (ConsoleForegroundColor(ConsoleColor.Red))
                {
                    Console.WriteLine($"ERROR: {e.Message}");
                }
                Console.WriteLine();
                Console.WriteLine(e.StackTrace);
                Console.WriteLine();

                if (System.Diagnostics.Debugger.IsAttached)
                {
                    System.Diagnostics.Debugger.Break();
                }
            }
        }

        public static async Task Execute(ISolver solver)
        {
            int year = solver.GetType().SolverYear();
            int day = solver.GetType().SolverDay();

            PrintSolverBanner(solver, year, day);

            var input = await Input.GetInput(year, day);
            int solutionIdx = 1;
            var sw = Stopwatch.StartNew();
            foreach (var solution in solver.Solve(input))
            {
                var time = sw.Elapsed;

                PrintSolution(solutionIdx++, solution, time);

                sw = Stopwatch.StartNew();
            }
            Console.WriteLine();
        }

        public static async Task Execute(IEnumerable<ISolver> solvers)
        {
            foreach (var solver in solvers)
            {
                await Execute(solver);
            }
        }

        private static void PrintProgramBanner()
        {
            var banner = @"
    __ _  __| __   _____ _ __ | |_  ___  / _  ___ ___   __| | ___ 
   / _` |/ _` \ \ / / _ | '_ \| __ / _ \| |_ / __/ _ \ / _` |/ _ \
  | (_| | (_| |\ V |  __| | | | |_| (_) |  _| (_| (_) | (_| |  __/
   \__,_|\__,_| \_/ \___|_| |_|\__ \___/|_|  \___\___/ \__,_|\___|

";

            Console.WriteLine();
            using (ConsoleForegroundColor(ConsoleColor.DarkRed))
            {
                Console.WriteLine(banner);
            }
            using (ConsoleForegroundColor(ConsoleColor.DarkGray))
            {
                Console.WriteLine("    Advent of Code | https://adventofcode.com/ ");
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        private static void PrintSolverBanner(ISolver solver, int year, int day)
        {
            var problemBanner = $"Problem {year} {day}";
            var boxTop = '═'.Repeat(problemBanner.Length).AsString();
            using (ConsoleForegroundColor(ConsoleColor.Green))
            {

                Console.WriteLine($"╔═{boxTop}═╗");
                Console.Write($"║ {problemBanner} ║");
            }
            using (ConsoleForegroundColor(ConsoleColor.DarkYellow))
            {
                Console.WriteLine($" >> {solver.Name} <<");
            }
            using (ConsoleForegroundColor(ConsoleColor.Green))
            {
                Console.WriteLine($"╚═{boxTop}═╝");
            }
            Console.WriteLine();
        }

        private static void PrintSolution(int solutionIdx, object solution, TimeSpan time)
        {
            using (ConsoleForegroundColor(ConsoleColor.White))
            {
                Console.Write($"  Answer {solutionIdx++}: ");
            }
            using (ConsoleForegroundColor(ConsoleColor.Yellow))
            {
                Console.WriteLine($"{solution}");
            }
            Console.WriteLine();
            using (ConsoleForegroundColor(ConsoleColor.Gray))
            {
                Console.WriteLine($"    calculation time: {time}");
            }
            Console.WriteLine();
        }


        public static Options ReadOptions(string[] args)
        {
            string AOC_DATA_DIR;
            AOC_DATA_DIR = Environment.GetEnvironmentVariable(nameof(AOC_DATA_DIR))
                    ?? throw new Exception($"Environment variable {nameof(AOC_DATA_DIR)} is unset!");

            string AOC_SESSION_COOKIE;
            AOC_SESSION_COOKIE = Environment.GetEnvironmentVariable(nameof(AOC_SESSION_COOKIE))
                    ?? throw new Exception($"Environment variable {nameof(AOC_SESSION_COOKIE)} is unset!");

            return new Options
            {
                SessionCookie = AOC_SESSION_COOKIE,
                DataDirectory = AOC_DATA_DIR
            };
        }

        private static IContainer BuildContainer(Options options)
        {
            var builder = new ContainerBuilder();

            var assemblies = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll")
                                .Select(Assembly.LoadFrom)
                                .Where(a => a.FullName.StartsWith("Kodfodrasz.AoC") && !a.FullName.Contains("Test", StringComparison.InvariantCultureIgnoreCase))
                                .ToArray();

            builder.RegisterAssemblyTypes(assemblies)
                    .AssignableTo<ISolver>()
                    .Named<ISolver>(t => $"{TypeConventions.SolverYear(t)}-{TypeConventions.SolverDay(t)}")
                    .AsImplementedInterfaces()
                    .AsSelf();

            builder.RegisterType<System.Net.Http.HttpClient>()
                   .AsSelf()
                   .OnActivating(activating => activating.Instance.DefaultRequestHeaders.Add("Cookie", $"session={options.SessionCookie}"))
                   .SingleInstance();

            return builder.Build();
        }

        private static ColoredOutput ConsoleForegroundColor(ConsoleColor color)
            => new ColoredOutput(color);

        private class ColoredOutput : IDisposable
        {
            private readonly ConsoleColor savedForegroundColor = Console.ForegroundColor;

            public ColoredOutput(ConsoleColor color)
            {
                Console.ForegroundColor = color;
            }

            public void Dispose() => Console.ForegroundColor = savedForegroundColor;
        }
    }
}
