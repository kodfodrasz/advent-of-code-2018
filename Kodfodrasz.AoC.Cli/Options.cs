﻿namespace Kodfodrasz.AoC.Cli {
    public class Options {
        public string SessionCookie { get; set; }
        public string DataDirectory { get; set; }
    }
}
