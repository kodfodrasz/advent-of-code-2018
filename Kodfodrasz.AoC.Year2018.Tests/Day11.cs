﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day11;
using Xunit;

namespace Kodfodrasz.AoC.Year2018.Tests
{
    public class Day11
    {
        [Theory]
        [InlineData(8, 3, 5, 4)]
        [InlineData(57, 122, 79, -5)]
        [InlineData(39, 217, 196, 0)]
        [InlineData(71, 101, 153, 4)]
        public void Day11_CalculateGrid_Test(int serial, int x, int y, int expectedPower)
        {
            var power = SolverForDay11.CalculateGridCell(serial, x, y);
            power.Should().Be(expectedPower);
        }

        [Fact]
        public void Day11_Answer1_Example()
        {
            var res = SolverForDay11.Answer1Extended(18);

            res.x.Should().Be(33);
            res.y.Should().Be(45);
        }

        [Fact(Skip = "Works, but sloooow (7min on my machine)")]
        public async Task Day11_Answer2_Example1()
        {
            var res = await SolverForDay11.Answer2Extended(18);

            res.x.Should().Be(90);
            res.y.Should().Be(269);
            res.value.Should().Be(113);
            res.kernelSize.Should().Be(16);
        }

        [Fact(Skip = "Works, but sloooow (7min on my machine)")]
        public async Task Day11_Answer2_Example2()
        {
            var res = await SolverForDay11.Answer2Extended(42);

            res.x.Should().Be(232);
            res.y.Should().Be(251);
            res.value.Should().Be(119);
            res.kernelSize.Should().Be(12);
        }

    }
}
