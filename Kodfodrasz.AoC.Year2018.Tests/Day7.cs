﻿using Xunit;
using FluentAssertions;
using System.Collections.Immutable;
using Kodfodrasz.AoC.Year2018.Day7;
using System.Linq;

namespace Kodfodrasz.AoC.Year2018.Tests
{
    public class Day7
    {
        static readonly string ExampleInput = @"
            Step C must be finished before step A can begin.
            Step C must be finished before step F can begin.
            Step A must be finished before step B can begin.
            Step A must be finished before step D can begin.
            Step B must be finished before step E can begin.
            Step D must be finished before step E can begin.
            Step F must be finished before step E can begin.
";

        ImmutableArray<(char step, char prerequisite)> ExampleData = ExampleInput.Split('\n').Where(StringTools.NotNullOrWhiteSpace)
            .Select(SolverForDay7.ParseAssemblyStep)
            .ToImmutableArray();

        [Fact]
        public void Day7_ParseAssemblyStep_Test()
        {
            var line = "Step C must be finished before step A can begin.";

            var res = SolverForDay7.ParseAssemblyStep(line);

            res.step.Should().Be('C');
            res.prerequisite.Should().Be('A');
        }

        [Fact(Skip = "TODO")]
        public void Day7_Answer1_Example()
        {
            var res = SolverForDay7.Answer1(ExampleData);

            res.Should().Be("CABDFE");
        }
    }
}
