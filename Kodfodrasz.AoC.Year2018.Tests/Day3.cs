﻿using Xunit;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day3;

namespace Kodfodrasz.AoC.Year2018.Tests {
    public class Day3
    {
        [Fact]
        public void InputLineIsProcessedCorrectly()
        {
            var answer = SolverForDay3.ParseClaim("#123 @ 31,23: 56x47");

            answer.Should().Be((
#pragma warning disable CS8123 // The tuple element name is ignored because a different name or no name is specified by the assignment target.
                id: 123,
                left: 31,
                top: 23,
                width: 56,
                height: 47
#pragma warning restore CS8123 // The tuple element name is ignored because a different name or no name is specified by the assignment target.
            ));
        }
    }
}
