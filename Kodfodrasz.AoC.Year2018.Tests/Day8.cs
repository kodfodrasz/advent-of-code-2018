﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day8;
using Xunit;

namespace Kodfodrasz.AoC.Year2018.Tests
{
    public class Day8
    {
        /*
         * 2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
         * A----------------------------------
         *     B----------- C-----------
         *                      D-----
         */
        static readonly string ExampleInput = @"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";

        IImmutableList<int> ExampleData = ExampleInput.Split('\n').Where(StringTools.NotNullOrWhiteSpace)
                                            .Select(SolverForDay8.ParseEncodedTreeDataLine)
                                            .Single();

        [Fact]
        public void Day8_ParseTreeDataLine_Test()
        {
            var line = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";

            var res = SolverForDay8.ParseEncodedTreeDataLine(line);

            res.Should().BeEquivalentTo(2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2);
        }

        [Fact]
        public void Day8_ParseTree_Test()
        {
            var nochildren = new List<TreeNode>();

            // A
            var tree = new TreeNode(
                children: new List<TreeNode> {
                    // B
                    new TreeNode(
                        nochildren,
                        new List<int>{ 10, 11, 12 }
                    ),
                    // C
                    new TreeNode(
                        children: new List<TreeNode> {
                            // D
                            new TreeNode(
                                nochildren,
                                new List<int>{ 99 })
                        },
                        new List<int>{ 2 })
                },
                metadata: new List<int> { 1, 1, 2 }
            );

            var res = SolverForDay8.ParseTree(ExampleData);

            res.Should().BeEquivalentTo(tree);
        }

        [Fact]
        public void Day8_Answer1_Example()
        {
            var res = SolverForDay8.Answer1(ExampleData);

            res.Should().Be(138);
        }

        [Fact]
        public void Day8_Answer2_Example()
        {
            var res = SolverForDay8.Answer2(ExampleData);

            res.Should().Be(66);
        }
    }
}
