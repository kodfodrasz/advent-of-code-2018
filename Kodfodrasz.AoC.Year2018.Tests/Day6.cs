﻿using Xunit;
using FluentAssertions;
using System.Collections.Immutable;
using Kodfodrasz.AoC.Year2018.Day6;
using System.Linq;

namespace Kodfodrasz.AoC.Year2018.Tests {
    public class Day6
    {
        static readonly string ExampleInput = @"
                    1, 1
                    1, 6
                    8, 3
                    3, 4
                    5, 5
                    8, 9
                    ";

        ImmutableArray<(int x, int y)> ExampleData = ExampleInput.Split('\n').Where(StringTools.NotNullOrWhiteSpace)
            .Select(SolverForDay6.ParseCoordinate)
            .ToImmutableArray();

        [Fact]
        public void Day6_Answer1_Example()
        {
            var res = SolverForDay6.Answer1(ExampleData);

            res.Should().Be(17);
        }

        [Fact]
        public void Day6_Answer2_Example()
        {
            var res = SolverForDay6.Answer2Calculator(ExampleData, 32);

            res.Should().Be(16);
        }
    }
}
