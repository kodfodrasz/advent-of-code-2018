﻿using System.Collections.Immutable;
using Xunit;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day2;

namespace Kodfodrasz.AoC.Year2018.Tests {
    public class Day2 {
        [Fact]
        public void ExampleIsProcessedCorrectly() {
            var input = ImmutableArray.Create(
                            "abcde",
                            "fghij",
                            "klmno",
                            "pqrst",
                            "fguij",
                            "axcye",
                            "wvxyz");

            var answer = SolverForDay2.Answer2(input);

            answer.Should().Be("fgij");
        }
    }
}
