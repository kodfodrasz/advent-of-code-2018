﻿using Xunit;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day5;

namespace Kodfodrasz.AoC.Year2018.Tests {
    public class Day5
    {
        [Fact]
        public void IsReversePolarityTest()
        {
            SolverForDay5.IsReversePolarity('a', 'A').Should().BeTrue("a A");
            SolverForDay5.IsReversePolarity('A', 'A').Should().BeFalse("A A");
            SolverForDay5.IsReversePolarity('a', 'a').Should().BeFalse("a a");
            SolverForDay5.IsReversePolarity('A', 'a').Should().BeTrue("A a");

            SolverForDay5.IsReversePolarity('a', 'B').Should().BeFalse("a B");
            SolverForDay5.IsReversePolarity('A', 'B').Should().BeFalse("A B");
            SolverForDay5.IsReversePolarity('a', 'b').Should().BeFalse("a b");
            SolverForDay5.IsReversePolarity('A', 'b').Should().BeFalse("A b");
        }

        [Fact]
        public void Answer1Test()
        {
            var res = SolverForDay5.Annihilate("dabAcCaCBAcCcaDA");

            res.Should().Be("dabCBAcaDA");
        }
    }
}
