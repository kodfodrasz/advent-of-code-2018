﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day12;
using Xunit;

namespace Kodfodrasz.AoC.Year2018.Tests
{
    public class Day12
    {
        static readonly string ExampleInput = @"
            initial state: #..#.#..##......###...###

            ...## => #
            ..#.. => #
            .#... => #
            .#.#. => #
            .#.## => #
            .##.. => #
            .#### => #
            #.#.# => #
            #.### => #
            ##.#. => #
            ##.## => #
            ###.. => #
            ###.# => #
            ####. => #
";

        static readonly IEnumerable<string> lines
            = ExampleInput.Split('\n', '\r')
                .Where(StringTools.NotNullOrWhiteSpace)
                .Select(s => s.Trim());

        static readonly ImmutableSortedSet<int> ExampleInitialState = SolverForDay12.ParseInitialState(lines);
        static readonly ImmutableDictionary<string, char> ExampleRules = SolverForDay12.ParseRules(lines);


        [Fact]
        public void Day12_ParseInitialState_Test()
        {
            var line = "initial state: #..#.#..##......###...###";
            var result = SolverForDay12.ParseInitialState(line);

            result.Should().Contain(new int[] { 0, 3, 5, 8, 9, 16, 17, 18, 22, 23, 24 });
        }

        [Fact]
        public void Day12_ParseRule_Test()
        {
            var line = ".#.## => #";
            (var pattern, var result) = SolverForDay12.ParseRule(line);

            pattern.Should().Be(".#.##");
            result.Should().Be('#');
        }

        [Fact]
        public void Day12_ParseRule_Test2()
        {
            var line = "###.. => .";
            (var pattern, var result) = SolverForDay12.ParseRule(line);

            pattern.Should().Be("###..");
            result.Should().Be('.');
        }

        [Fact]
        public void Day12_Iterate_Test()
        {
            var iteration1 = SolverForDay12.Iterate(ExampleInitialState, ExampleRules);
            var iteration2 = SolverForDay12.Iterate(iteration1, ExampleRules);

            iteration1.Should().Contain(new int[] { 0, 4, 9, 15, 18, 21, 24 });
            iteration2.Should().Contain(new int[] { 0, 1, 4, 5, 9, 10, 15, 18, 21, 24, 25 });
        }

        [Fact]
        public void Day12_Answer1_Example()
        {
            SolverForDay12.Answer1(ExampleInitialState, ExampleRules).Should().Be(325);
        }

        [Fact(Skip = "TODO")]
        public async Task Day12_Answer2_Example()
        {
            throw new NotImplementedException();
        }
    }
}
