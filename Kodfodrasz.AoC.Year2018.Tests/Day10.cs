﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day10;
using Xunit;

namespace Kodfodrasz.AoC.Year2018.Tests
{
    public class Day10
    {
        static readonly string ExampleInput = @"
            position=< 9,  1> velocity=< 0,  2>
            position=< 7,  0> velocity=<-1,  0>
            position=< 3, -2> velocity=<-1,  1>
            position=< 6, 10> velocity=<-2, -1>
            position=< 2, -4> velocity=< 2,  2>
            position=<-6, 10> velocity=< 2, -2>
            position=< 1,  8> velocity=< 1, -1>
            position=< 1,  7> velocity=< 1,  0>
            position=<-3, 11> velocity=< 1, -2>
            position=< 7,  6> velocity=<-1, -1>
            position=<-2,  3> velocity=< 1,  0>
            position=<-4,  3> velocity=< 2,  0>
            position=<10, -3> velocity=<-1,  1>
            position=< 5, 11> velocity=< 1, -2>
            position=< 4,  7> velocity=< 0, -1>
            position=< 8, -2> velocity=< 0,  1>
            position=<15,  0> velocity=<-2,  0>
            position=< 1,  6> velocity=< 1,  0>
            position=< 8,  9> velocity=< 0, -1>
            position=< 3,  3> velocity=<-1,  1>
            position=< 0,  5> velocity=< 0, -1>
            position=<-2,  2> velocity=< 2,  0>
            position=< 5, -2> velocity=< 1,  2>
            position=< 1,  4> velocity=< 2,  1>
            position=<-2,  7> velocity=< 2, -2>
            position=< 3,  6> velocity=<-1, -1>
            position=< 5,  0> velocity=< 1,  0>
            position=<-6,  0> velocity=< 2,  0>
            position=< 5,  9> velocity=< 1, -2>
            position=<14,  7> velocity=<-2,  0>
            position=<-3,  6> velocity=< 2, -1>
";

        ImmutableArray<((int x, int y) position, (int x, int y) velocity)> 
            ExampleData
                = ExampleInput
                    .Split('\n', '\r')
                    .Where(StringTools.NotNullOrWhiteSpace)
                    .Select(s => s.Trim())
                    .Select(SolverForDay10.ParseLine)
                    .ToImmutableArray();

        [Fact]
        public void Day10_ParseLine_Test()
        {
            SolverForDay10.ParseLine("position=<-4,  3> velocity=< 2,  0>")
                .Should().BeEquivalentTo(((-4, 3), (2, 0)));
            SolverForDay10.ParseLine("position=<10, -3> velocity=<-1,  1>")
                .Should().BeEquivalentTo(((10, -3), (-1, 1)));
        }

        [Fact]
        public void Day10_Render_Test()
        {
            string expected = @"
........#.............
................#.....
.........#.#..#.......
......................
#..........#.#.......#
...............#......
....#.................
..#.#....#............
.......#..............
......#...............
...#...#.#...#........
....#..#..#.........#.
.......#..............
...........#..#.......
#...........#.........
...#.......#..........
";

            var rendered = SolverForDay10.Render(ExampleData);

            rendered.Should().Be(expected);
        }

        [Fact]
        public void Day10_Answer1_Example()
        {
            string expected = @"
#   #  ###
#   #   # 
#   #   # 
#####   # 
#   #   # 
#   #   # 
#   #   # 
#   #  ###
";

            var rendered = SolverForDay10.Answer1(ExampleData);

            rendered.Should().Be(expected);
        }

        [Fact(Skip = "TODO")]
        public async Task Day10_Answer2_Example()
        {
            throw new NotImplementedException();
        }
    }
}
