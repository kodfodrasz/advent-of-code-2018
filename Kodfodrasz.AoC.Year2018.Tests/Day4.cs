﻿using Xunit;
using FluentAssertions;
using Kodfodrasz.AoC.Year2018.Day4;

namespace Kodfodrasz.AoC.Year2018.Tests {
    public class Day4
    {
        [Fact]
        public void InputLineIsProcessedCorrectly()
        {
            var answer = SolverForDay4.ParseLogLine("[1518-11-01 00:00] Guard #10 begins shift");

            answer.Should().Be((
#pragma warning disable CS8123 // The tuple element name is ignored because a different name or no name is specified by the assignment target.
                timestamp: new System.DateTime(1518,11,01,00,00,00),
                text: "Guard #10 begins shift"
#pragma warning restore CS8123 // The tuple element name is ignored because a different name or no name is specified by the assignment target.
            ));
        }
    }
}
